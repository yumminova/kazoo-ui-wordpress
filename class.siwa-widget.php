<?php
/**
 * @package Siwa
 */
class Siwa_Account_Widget extends WP_Widget {

	function __construct() {
		load_plugin_textdomain( 'siwa' );
		
		parent::__construct(
			'siwa_account_widget',
			__( 'Siwa Account Widget' , 'siwa'),
			array( 'description' => __( 'Set account info box' , 'siwa') )
		);

		
	}


	function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $args['before_widget'];
		
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'];
			echo esc_html( $instance['title'] );
			echo $args['after_title'];
		}
?>

<div id="ws-topbar" class="ws-topbar">
  <ul class="nav secondary-nav links"></ul>
    <!--li data-weight="05" data-link="stats" class="dropdown link"> <a class="dropdown-toggle">
      <div id="statistics_navbar">
        <div data-name="credits" class="stat_wrapper clickable">
          <div class="amount_wrapper">
            <div class="red" id="credits_label">$ 0.00</div>
            <div class="add_credits_label">(add credit)</div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="clear"> </div>
      </a>
      <ul class="dropdown-menu">
      </ul>
    </li>
    <li data-weight="10" data-link="nav" class="dropdown link" data-dropdown="dropdown"> <a class="dropdown-toggle" style="padding: 0px;">
      <div id="myaccount_navbar">
        <div id="myaccount_info">
          <div class="arrow"></div>
          <div>
            <div>siwa siwa</div>
            <div> <a class="masquerade" href="#">siwa</a> </div>
          </div>
        </div>
      </div>
      </a>
      <ul class="dropdown-menu" style="width: 233px; margin-right: -73px;">
        <li data-masqueradable="true" data-sublink="switch_account" data-weight="05" data-category="" class="sublink"> <a href="#"> Switch account </a> </li>
        <li data-masqueradable="false" data-sublink="perso" data-weight="10" data-category="" class="sublink"> <a href="#"> My Account </a> </li>
        <li data-masqueradable="false" data-sublink="billing" data-weight="15" data-category="" class="sublink"> <a href="#"> Billing </a> </li>
        <li data-masqueradable="true" data-sublink="report" data-weight="17" data-category="" class="sublink"> <a href="#"> Report </a> </li>
        <li data-masqueradable="false" data-sublink="app_store" data-weight="20" data-category="" class="sublink"> <a href="#"> App Store </a> </li>
        <li data-masqueradable="true" data-sublink="logout" data-weight="25" data-category="" class="sublink"> <a href="#"> Sign out </a> </li>
      </ul>
    </li>
    <li data-weight="15" data-link="help" class="dropdown link"> <a class="dropdown-toggle" style="padding: 0px;">
      <div id="myaccount_help"> <a id="help_link" target="_blank" href="http://wiki.2600hz.com"></a> </div>
      </a>
      <ul class="dropdown-menu">
      </ul>
    </li-->
 
</div>

<?php
		echo $args['after_widget'];
	}
}



function siwa_register_widgets() {
	register_widget( 'Siwa_Account_Widget' );
}

add_action( 'widgets_init', 'siwa_register_widgets' );

var URL_PARTS = window.location.href.match(/^([^\?#]+)[\?#]*([^#]*).*$/).slice(1),
URL = URL_PARTS[0],
URL_DATA = function() {
		var url_data_parts = (URL_PARTS[1]) ? URL_PARTS[1].split('&') : [],
				url_data = {},
				data;

		for(var i = 0; i < url_data_parts.length; i++) {
				data = url_data_parts[i].split('=');
				
				url_data[data[0]] = data[1];
		}

		return url_data;
}();
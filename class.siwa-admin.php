<?php
class Siwa_Admin
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;
    
    private $menu_name = 'siwa-navigation';
    
    private $menu_id;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );

        
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'Siwa', 
            'manage_options', 
            'siwa-setting-admin', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'siwa_option_name' );
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2>Siwa</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'siwa_option_group' );   
                do_settings_sections( 'siwa-setting-admin' );
                submit_button(); 
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'siwa_option_group', // Option group
            'siwa_option_name', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Configuration', // Title
            array( $this, 'print_section_info' ), // Callback
            'siwa-setting-admin' // Page
        );  

        add_settings_field(
            'api_url', // ID
            'Api Url', // Title 
            array( $this, 'api_url_callback' ), // Callback
            'siwa-setting-admin', // Page
            'setting_section_id' // Section           
        );      

        add_settings_field(
            'account_id', 
            'Account Id', 
            array( $this, 'account_id_callback' ), 
            'siwa-setting-admin', 
            'setting_section_id'
        ); 
        
        add_settings_field(
            'auth_token', 
            'Auth Token', 
            array( $this, 'auth_token_callback' ), 
            'siwa-setting-admin', 
            'setting_section_id'
        );      
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['api_url'] ) )
            $new_input['api_url'] = $input['api_url'] ;

        if( isset( $input['account_id'] ) )
            $new_input['account_id'] = sanitize_text_field( $input['account_id'] );
        
        if( isset( $input['auth_token'] ) )
            $new_input['auth_token'] = sanitize_text_field( $input['auth_token'] );        

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your settings below:';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function api_url_callback()
    {
        printf(
            '<input type="url" id="api_url" name="siwa_option_name[api_url]" value="%s" style="200px;" />',
            isset( $this->options['api_url'] ) ? esc_attr( $this->options['api_url']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function account_id_callback()
    {
        printf(
            '<input type="text" id="account_id" name="siwa_option_name[account_id]" value="%s" />',
            isset( $this->options['account_id'] ) ? esc_attr( $this->options['account_id']) : ''
        );
    }
    
    /** 
     * Get the settings option array and print one of its values
     */
    public function auth_token_callback()
    {
        printf(
            '<input type="text" id="auth_token" name="siwa_option_name[auth_token]" value="%s" />',
            isset( $this->options['auth_token'] ) ? esc_attr( $this->options['auth_token']) : ''
        );
    }
}